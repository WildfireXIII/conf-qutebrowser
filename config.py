#c.general.editor = "vim -f '{}'"
c.editor.command = ["urxvt", "-e", "bash", "-c", "vim -f {file}"]

c.statusbar.padding = {'bottom':2, 'top':2, 'right':2, 'left':2}

c.tabs.indicator.width = 0

c.tabs.title.format = "{title}"
c.tabs.title.alignment = "center"

c.tabs.padding = {'bottom':4, 'top':4, 'right':4, 'left':4}

#c.content.pdfjs = True

c.colors.statusbar.normal.bg = "#111111"
c.colors.statusbar.url.success.https.fg ="#51bd5d"
c.colors.tabs.odd.bg = "#444444"
c.colors.tabs.even.bg = "#333333"


c.fonts._monospace = ["Droid Sans Mono", "Terminus", "Monospace", "DejaVu Sans Mono", "Monaco", "Bitstream Vera Sans Mono", "Andale Mono", "Courier New", "Courier", "Liberation Mono", "monospace", "Fixed", "Consolas", "Terminal"]
c.fonts.completion.entry = "8pt Droid Sans Mono"
c.fonts.completion.category = "8pt Droid Sans Mono"
c.fonts.tabs = "8pt Droid Sans Mono"
c.fonts.statusbar = "8pt Droid Sans Mono"
c.fonts.downloads = "8pt Droid Sans Mono"
c.fonts.debug_console = "8pt Droid Sans Mono"

c.downloads.position = "bottom"

config.bind(";", "set-cmd-text :", mode="normal")
